function C = MatrixMatrix(A,B) %Multiplies a Matrix(nxm) another matrix(mxk)
    for i = 1:size(A,1)
        for j = 1:size(B,2)
            C(i,j) = sum(A(i,:).*B(:,j)'); %multiple components of row i in A by components of column j in B then sum
                                          %we use x' so  .* works.
        end
    end
    Joshua Arenson
