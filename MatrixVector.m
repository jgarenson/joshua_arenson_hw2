function b = MatrixVector(A,x) %Multiplies a Matrix(nxm) a vector(nx1)
    for i = 1:size(A,1)
        b(i,1) = sum(A(i,:).*x'); %multiple components of row i in A by components of x. we use x' so  .* works.
    end
    Joshua Arenson
Joshua Arenson
