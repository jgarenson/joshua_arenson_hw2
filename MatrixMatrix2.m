function C = MatrixMatrix2(A,B) %Multiplies a Matrix(nxn) a Matrix(nxk)
    for i = 1:size(B,2)
        C(:,i) = MatrixVector(A,B(:,i)); %Multiplies Matrix A by column i of B to get column i of C.
    end
    Joshua Arenson
