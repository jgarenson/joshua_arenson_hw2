% MatrixVector.m Contains the solution to 6a. It takes an input of a nxm 
% matrix A and mx1 vector x and produces the nx1 vector b.
% 
% MatrixMatrix.m Contains the solution to 6b. It takes an input of a nxm 
% matrix A and mxk matrix B and produces the nxk matrix C.
% 
% MatrixMatrix2.m Contains the solution to 6c. It takes input of a nxm 
% matrix A and mxk matrix B to produce nxk matrix C. Unlike MatrixMatrix 
% it makes use of MatrixVector to compute each column vector of C. Joshua Arenson
